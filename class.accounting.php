<?php
	/**
	 * @file
	 *		class.accounting.php
	 * 
	 * Accounting Plugin for Codiad
	 * 
	 * @author
	 *		V.Lopez		<v.lopez@caixadesoft.es>
	 * 
	 * @version
	 *		2019.06.21 - V.Lopez -> Preliminary version
	 *		2019.08.06 - V.Lopez -> Adopting Object POLLER.
	 *								New Database Table.
	 * 
	 */

	require_once('../../common.php');

	/**
	 * Accounting
	 */
	class Accounting
	{
		/**
		 * 
		 * 
		 * 
		 *  
		 */
		public static function poller( $data )
		{
			global $sql;
			
			error_log( "PLG_ACCOUNTING:X:poller");
			
			$positions = json_decode( $data , true );
	
			$query = "";
			$bind_variables = array();
			
			if( json_last_error() == JSON_ERROR_NONE ) 
			{
				foreach ( $positions as $path => $cursor ) 
				{
					// --- Calculate Position Hash ---
					$crcCursor = dechex( crc32( json_encode( $cursor ) ) );
					
					$query.= $sql->selectQuery([
													'mysql'	=> "INSERT INTO plg_fus ( plgfus_usr , plgfus_checksum , plgfus_file , plgfus_position , plgfus_access , plgfus_time ) " .
																	"VALUES ( ? , ? , ? , ? , ? , ? ) " .
																	"ON DUPLICATE KEY UPDATE " .
																		"plgfus_time = IF( plgfus_position <> ? , plgfus_time + 10 , plgfus_time ) , " .
																		"plgfus_position = ? ;",
													
													'pgsql'	=> "INSERT INTO plg_fus ( plgfus_usr , plgfus_checksum , plgfus_file , plgfus_position , plgfus_access , plgfus_time ) " .
																	"VALUES ( ? , ? , ? , ? , ? , ? ) " .
																	"ON CONFLICT WHERE plgfus_position <> ? " .
																		"DO UPDATE SET plgfus_time = plgfus_time + 10 , plgfus_position = ? ;"
											]);
							
					array_push( $bind_variables , $_SESSION['usr']['usr_id'] , md5( $path ) , $path , $crcCursor , time() , 0 , $crcCursor , $crcCursor );
					
					error_log( md5( $path ) . "=" . $crcCursor );
				}
				
				$return = $sql->query( $query , $bind_variables , 0 , "rowCount" );
			}
			
			return $return;
		}
	}


# Accounting Codiad Plugin 

Plugin for accounting file usage per user.

Need a table in a database with these fields:

MYSQL

```
CREATE TABLE `plgfus` (
  `plgfus_usr` int(10) unsigned NOT NULL,
  `plgfus_prj` int(10) unsigned,
  `plgfus_checksum` char(32) COLLATE utf8_bin NOT NULL,
  `plgfus_path` char(255) COLLATE utf8_bin NOT NULL,
  `plgfus_position` char(8) COLLATE utf8_bin NOT NULL,
  `plgfus_access` int(10) unsigned NOT NULL,
  `plgfus_time` int(10) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `plgusrchk` (`plgfus_usr`,`plgfus_prj`,`plgfus_checksum`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
```

POSTGRESQL
```
CREATE TABLE plgfus (
  plgfus_usr integer NOT NULL,
  plgfus_prj integer NOT NULL,
  plgfus_checksum char(32),
  plgfus_path varchar(255) NOT NULL,
  plgfus_position varchar(8) NOT NULL,
  plgfus_access integer NOT NULL,
  plgfus_time integer NOT NULL DEFAULT 0,
  UNIQUE (plgfus_usr, plgfus_prj , plgfus_checksum )
);
```


/**
 * @file 
 *		init.js
 * 
 * Accounting Plugin for codiad
 * 
 * @author
 *		V.Lopez		<v.lopez@caixadesoft.es>
 * 
 * @version
 *		2019.06.21 - Preliminary version
 */

( function( global, $ ) 
{
	var codiad = global.codiad;
	var scripts= document.getElementsByTagName('script');
	var path = scripts[scripts.length-1].src.split('?')[0];
	var curpath = path.split('/').slice(0, -1).join('/')+'/';

	// --- Start Plugin ---
	$( function() 
		{ 
			codiad.accounting.init();
		});
	
	codiad.accounting = 
	{
		controller: curpath + 'controller.php',
		path: '',

		init: function() 
		{
			$.ajax({
						type: 'POST',
						url: codiad.accounting.controller + '?action=poller',
						data: {},
						success: function( data ) 
								{
									codiad.accounting.path = JSON.parse( data ).data;
								},
					});
		},
		
		pollSend: function()
		{
			let result = JSON.stringify( codiad.active.positions );
			
			return {
						'path'	: codiad.accounting.path,
						'data'	: result
					};
		},
		
		pollRecv: function( data )
		{
			console.log( data );
		}
	}
})( this, jQuery );

